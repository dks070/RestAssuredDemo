package getRequest;

import org.junit.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import junit.framework.Assert;

public class GetData {

	@Test
	public void testResponcecode()
	{
		
	Response resp =	RestAssured.get("http://samples.openweathermap.org/pollution/v1/co/0.0,10.0/current.json?appid=b1b15e88fa797225412429c1c50c122a1");
	
	int code=resp.getStatusCode();
	System.out.println("status code is "+code);
	
	Assert.assertEquals(code, 200);
	}
	
	@Test
	public void testBody()
	{
		
	Response resp=	RestAssured.get("http://samples.openweathermap.org/pollution/v1/co/0.0,10.0/current.json?appid=b1b15e88fa797225412429c1c50c122a1");
	
	String data=resp.asString();
	System.out.println("Data  is "+data);
	System.out.println("Responce time" +resp.getTime());
	
	}
}
